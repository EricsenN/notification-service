from django.db import models
from django.db.models import Count


class MailingList(models.Model):
    sending_datetime = models.DateTimeField(verbose_name='Sending DateTime')
    text = models.TextField(verbose_name='Message Text')
    property_filter = models.CharField(verbose_name='Code or Tag')
    end_sending_datetime = models.DateTimeField(verbose_name='End of Sending Time')

    class Meta:
        verbose_name = 'Mailing List'
        verbose_name_plural = 'Mailings List'

    def __str__(self):
        return f'Mailing List - {self.pk}'

    @property
    def get_messages_count(self):
        messages = self.messages.values('status').annotate(count=Count('status')).order_by()
        return messages
