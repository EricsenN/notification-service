from .models import MailingList
from .tasks import send_message

from client.models import ClientModel
from message.models import MessageModel

from django.db.models.signals import post_save
from django.db.models import Q
from django.dispatch import receiver
from django.utils import timezone


@receiver(post_save, sender=MailingList, dispatch_uid='create_message')
def create_message(sender, instance, **kwargs):
    mailing = MailingList.objects.get(pk=instance.pk)
    clients = ClientModel.objects.filter(Q(mobile_operator_code=mailing.property_filter)
                                         | Q(tag=mailing.property_filter)).all()

    for client in clients:
        new_message = MessageModel.objects.create(
            mailing_id=mailing,
            client_id=client
        )
        data = {
            'id': new_message.pk,
            'phone': client.phone_number,
            'text': mailing.text
        }
        mailing_id = mailing.id
        client_id = client.id

        if mailing.sending_datetime < timezone.now() < mailing.end_sending_datetime:
            send_message.apply_async((data, mailing_id, client_id),
                                     expires=mailing.end_sending_datetime)
        else:
            send_message.apply_async((data, mailing_id, client_id),
                                     eta=mailing.sending_datetime,
                                     expires=mailing.end_sending_datetime)

