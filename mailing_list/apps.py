from django.apps import AppConfig


class MailingListConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailing_list'

    def ready(self):
        import mailing_list.signals
