import requests
import os

from .models import MailingList

from client.models import ClientModel
from message.models import MessageModel
from celery import shared_task
from django.utils import timezone
from dotenv import load_dotenv

load_dotenv()


@shared_task(bind=True)
def send_message(self, data, mailing_id, client_id):
    mailing = MailingList.objects.get(pk=mailing_id)
    client = ClientModel.objects.get(pk=client_id)

    url = os.getenv('URL')
    token = os.getenv('TOKEN')
    try:
        requests.post(url=url + '/' + str(data['id']),
                      json=data,
                      headers={
                        'Authorization': f"Bearer {token}",
                        "Content-Type": "application/json",
                    })
        message = MessageModel.objects.filter(pk=data['id']).update(status=True, creation_time=timezone.now())
    except Exception as exc:
        return self.retry(exc=exc)

