from .models import MailingList

from django.contrib import admin


@admin.register(MailingList)
class MailingListAdmin(admin.ModelAdmin):
    list_display = ('pk', 'sending_datetime', 'text', 'property_filter', 'end_sending_datetime', 'get_messages_count')
