from .models import MailingList
from .serializers import MailingListCreateSerializer, MailingListSerializer, MailingListDetailSerializer

from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView


class MailingListCreate(CreateAPIView):
    queryset = MailingList.objects.all()
    serializer_class = MailingListCreateSerializer


class MailingListView(ListAPIView):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer


class MailingListDetailView(RetrieveUpdateDestroyAPIView):
    queryset = MailingList.objects.all()
    serializer_class = MailingListDetailSerializer
