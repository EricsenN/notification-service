from .models import MailingList

from rest_framework import serializers
from message.serializers import MessageSerializer


class MailingListCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = ['sending_datetime', 'text', 'property_filter', 'end_sending_datetime']

    def validate(self, attrs):
        if attrs['sending_datetime'] > attrs['end_sending_datetime']:
            raise serializers.ValidationError('You entered the wrong time')
        return attrs


class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = ['sending_datetime', 'text', 'property_filter', 'end_sending_datetime', 'get_messages_count']


class MailingListDetailSerializer(serializers.ModelSerializer):
    messages = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = MailingList
        fields = ['sending_datetime', 'text', 'property_filter', 'end_sending_datetime', 'messages']
