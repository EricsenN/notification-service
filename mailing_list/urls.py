from .views import MailingListCreate, MailingListView, MailingListDetailView

from django.urls import path

urlpatterns = [
    path('api/mailing-list/add/', MailingListCreate.as_view()),
    path('api/mailing-list/', MailingListView.as_view()),
    path('api/mailing-list/<int:pk>', MailingListDetailView.as_view()),
]