from .models import MessageModel

from rest_framework import serializers


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageModel
        fields = ['creation_time', 'status', 'mailing_id', 'client_id']
