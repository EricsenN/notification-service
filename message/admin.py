from .models import MessageModel

from django.contrib import admin


@admin.register(MessageModel)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('pk', 'creation_time', 'status', 'mailing_id', 'client_id')
