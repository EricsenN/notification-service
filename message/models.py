from client.models import ClientModel
from mailing_list.models import MailingList

from django.db import models


class MessageModel(models.Model):
    creation_time = models.DateTimeField(verbose_name='Creation Time', blank=True, null=True)
    status = models.BooleanField(default=False, verbose_name='Status')
    mailing_id = models.ForeignKey(MailingList, on_delete=models.CASCADE, related_name='messages')
    client_id = models.ForeignKey(ClientModel, on_delete=models.CASCADE, related_name='messages')

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'

    def __str__(self):
        return f'Message {self.pk}. Sent - {self.status}. Created in {self.creation_time}. Client - {self.client_id}'
