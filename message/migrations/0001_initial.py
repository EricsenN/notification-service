# Generated by Django 4.2.1 on 2023-05-05 16:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('mailing_list', '0001_initial'),
        ('client', '0002_alter_clientmodel_phone_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='MessageModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_time', models.DateTimeField(auto_now_add=True, verbose_name='Creation Time')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
                ('client_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='client.clientmodel')),
                ('mailing_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='mailing_list.mailinglist')),
            ],
            options={
                'verbose_name': 'Message',
                'verbose_name_plural': 'Messages',
            },
        ),
    ]
