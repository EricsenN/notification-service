import pytz

from django.db import models


class ClientModel(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(max_length=11, unique=True,
                                    verbose_name='Phone Number')
    mobile_operator_code = models.CharField(max_length=4, verbose_name='Mobile Operator Code')
    tag = models.CharField(max_length=254, verbose_name='Simple Tag')
    timezone = models.CharField(max_length=32, choices=TIMEZONES,
                                default='UTC')

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'

    def __str__(self):
        return f'{self.phone_number}'
