from .models import ClientModel

from rest_framework import serializers


class ClientCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientModel
        fields = ['phone_number', 'tag', 'timezone']

    def validate(self, attrs):
        try:
            int_number = int(attrs['phone_number'])
            if len(attrs['phone_number']) != 11:
                raise serializers.ValidationError('Phone number should consists only of numbers with length 11 or start from 7!')
            if not attrs['phone_number'].startswith('7'):
                raise serializers.ValidationError('Phone number should consists only of numbers with length 11 or start from 7!')
            return attrs
        except:
            raise serializers.ValidationError('Phone number should consists only of numbers with length 11 or start from 7!')

    def create(self, validated_data):
        try:
            new_client = ClientModel.objects.create(phone_number=validated_data['phone_number'],
                                                    mobile_operator_code=validated_data['phone_number'][:4],
                                                    tag=validated_data['tag'],
                                                    timezone=validated_data['timezone'])
            new_client.save()
            return new_client
        except:
            raise serializers.ValidationError('Client with this phone number already exists!')


class ClientSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(read_only=True)
    mobile_operator_code = serializers.CharField(read_only=True)

    class Meta:
        model = ClientModel
        fields = ['phone_number', 'mobile_operator_code', 'tag', 'timezone']

