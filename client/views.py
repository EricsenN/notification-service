from .serializers import ClientCreateSerializer, ClientSerializer
from .models import ClientModel

from rest_framework.generics import CreateAPIView, RetrieveUpdateDestroyAPIView


class ClientCreate(CreateAPIView):
    queryset = ClientModel.objects.all()
    serializer_class = ClientCreateSerializer


class ClientView(RetrieveUpdateDestroyAPIView):
    queryset = ClientModel.objects.all()
    serializer_class = ClientSerializer
