from .models import ClientModel

from django.contrib import admin


@admin.register(ClientModel)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('pk', 'phone_number', 'mobile_operator_code', 'tag', 'timezone')
