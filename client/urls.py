from .views import ClientCreate, ClientView

from django.urls import path


urlpatterns = [
    path('api/client/add/', ClientCreate.as_view()),
    path('api/client/<int:pk>', ClientView.as_view()),
]
