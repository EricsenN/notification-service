FROM python:3.8

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /notification_service

COPY requirements.txt /notification_service/requirements.txt

RUN pip install --upgrade pip
RUN pip install -r /notification_service/requirements.txt

COPY . /notification_service/